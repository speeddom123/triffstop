const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth')
const Event = require('../Models/Item')
const mangoose = require('mongoose');
const { json } = require('body-parser');
const Multer = require('multer');
const multer = require('multer');
const picStorage = Multer.diskStorage({
    destination: function(req,file,cb){
        cb(null,'./uploads/')
    },
    filename: function(req,file,cb){
        cb(null, Date.now() + file.originalname )
    }
})
const upload = multer({storage: picStorage});




router.get('/', (req, res ,next) => {
    Event.find()
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});
router.get('/category/items/:id', (req, res ,next) => {
    const categoryId = req.params.id;
    Event.find({categoryId : categoryId})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});
router.get('/user/items/:id', (req, res ,next) => {
    const UserId = req.params.id;
    Event.find({userId : UserId})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});

router.post('/', checkAuth ,upload.single('pic'),(req, res ,next) => {
  
    const events = new Event(
        {
            _id: new mangoose.Types.ObjectId(),
           
            name: req.body.name,
            pic: req.file.filename ,
            condition: req.body.condition,
            description: req.body.description,
            returnPolicy: req.body.returnPolicy,
            qty: req.body.qty,
            price: req.body.price,
            userId:  req.body.userId,
            categoryId:  req.body.categoryId
           
        }
    );
    events.save()
    .then(result =>{
        console.log(result);
        res.status(201).json(result)
    })
    .catch(err => {
        console.log(err)
        res.status(500).json(err)
    }
    )
    ;
  
});

router.get('/:id', (req, res ,next) => {
    const eventID = req.params.id;
    Event.findById({_id:eventID})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
    
   
});


router.patch('/:id', checkAuth,upload.single('pic') ,(req, res ,next) => {
    const eventID = req.params.id;
    Event.update({_id:eventID}, {$set :{
        name: req.body.name,
        pic: req.file.filename,
        condition: req.body.condition,
        description: req.body.description,
        returnPolicy: req.body.returnPolicy,
        qty: req.body.qty,
        price: req.body.price

    }})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(500).json({error:err});
              } 
              );
   
});

router.delete('/:id', checkAuth,(req, res ,next) => {
    
    Event.remove({_id : req.params.id})
    .exec()
    .then(result => {
        console.log(result);

        res.status(200).json(result);
    })
    .catch(err =>{
        console.log(err);
        res.status(500),json9(err);
    })


});



module.exports = router;