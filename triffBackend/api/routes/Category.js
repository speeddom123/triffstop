const express = require('express');
const router = express.Router();
const Category = require('../Models/Category')
const mangoose = require('mongoose');
const { json } = require('body-parser');
const Multer = require('multer');
const multer = require('multer');
const picStorage = Multer.diskStorage({
    destination: function(req,file,cb){
        cb(null,'./uploads/')
    },
    filename: function(req,file,cb){
        cb(null, Date.now() + file.originalname)
    }
})

const upload = multer({storage: picStorage});




router.get('/', (req, res ,next) => {
    Category.find()
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});

router.post('/', upload.single('pic'),(req, res ,next) => {
  
    const cat = new Category(
        {
            _id: new mangoose.Types.ObjectId(),
           
            name: req.body.name,
            pic: req.file.filename,
            
           
        }
    );
    cat.save()
    .then(result =>{
        console.log(result);
        res.status(201).json(result)
    })
    .catch(err => {
        console.log(err)
        res.status(500).json(err)
    }
    )
    ;
  
});

router.get('/:id', (req, res ,next) => {
    const eventID = req.params.id;
    Category.findById({_id:eventID})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
    
   
});

router.patch('/:id', upload.single('pic'),(req, res ,next) => {
    const eventID = req.params.id;
    Category.update({_id:eventID}, {$set :{
        name: req.body.name,
            pic: req.file.filename,

    }})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});

router.delete('/:id', (req, res ,next) => {
    
    Category.remove({_id : req.params.id})
    .exec()
    .then(result => {
        console.log(result);

        res.status(200).json(result);
    })
    .catch(err =>{
        console.log(err);
        res.status(500),json9(err);
    })


});



module.exports = router;