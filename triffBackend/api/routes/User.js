const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth')
const Organiser = require('../Models/User')
const mangoose = require('mongoose');
const { json } = require('body-parser');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


router.get('/', (req, res ,next) => {
    Organiser.find()
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});

router.post('/signup', (req, res ,next) => {
   
    Organiser.find({email : req.body.email})
    .exec()
    .then( user => {
        if(user.length >= 1){
            return res.status(409).json({
                message: 'Email already exist in db'
            });
        }else
        {
            bcrypt.hash( req.body.password, 10 ,(err,hash) =>{
                if(err){
                    return res.status(500).json({
                        error: err,
                        message: "error at hash"
                    })
                }else{
                    const organiser = new Organiser(
                        {
                            _id: new mangoose.Types.ObjectId(),
                            userName: req.body.userName,
                            email: req.body.email,
                            password: hash,
                            fname: req.body.fname,
                            lname: req.body.lname,
                            address: req.body.address,
                            phoneNumber: req.body.phoneNumber
                            
                          
                           
                        }
                    );
                    organiser.save()
                    .then(result =>{
                        console.log(result);
                        res.status(201).json(result)
                    })
                    .catch(err => {
                        console.log(err)
                        res.status(503).json(err)
                    }
                    )
                    ;
                }
            }) 
        }
    })
});



router.post('/login', (req, res ,next) => {
    Organiser.find({email : req.body.email})
    .exec()
    .then( user =>{
        if(user.length <1){
            return res.status(401).json({
                message: 'Authprization failee'
            });
        }
        bcrypt.compare(req.body.password , user[0].password,(err,result)=>{
            if(err){
                return res.status(401).json({
                    message: 'Authprization failee'
                });
            }
            if(result){
                const token =jwt.sign({
                    email: user[0].email,
                    userid: user[0]._id
                },process.env.JWT_KEY,{
                    expiresIn: "1hr"
                }
                
                );
                user[0].token = token
                return res.status(200).json({
                    message: 'Authprization succesful',
                    user: user,
                    token: token
                    
                });
            }
            res.status(401).json({
                message: 'Authprization failee'
        })
    } )
    .catch();






})
})

    


   
    
                   
        
router.get('/:id', (req, res ,next) => {
    const organiserID = req.params.id;
    Organiser.findById({_id:organiserID})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
    
   
});


router.patch('/:id',checkAuth, (req, res ,next) => {
    const organiserID = req.params.id;
    Organiser.update({_id:organiserID}, {$set :{
        userName: req.body.userName,
        email: req.body.email,
        password: hash,
        fname: req.body.fname,
        lname: req.body.lname,
        address: req.body.address,
        phoneNumber: req.body.phoneNumber,

    }})
    .exec()
    .then(doc =>
         {
             console.log(doc);
             res.status(200).json(doc);
            })
         .catch(
              err =>{
                console.log(err);
                 res.status(5000).json({error:err});
              } 
              );
   
});

router.delete('/:id',checkAuth, (req, res ,next) => {
    
    Organiser.remove({_id : req.params.id})
    .exec()
    .then(result => {
        console.log(result);

        res.status(200).json(result);
    })
    .catch(err =>{
        console.log(err);
        res.status(500),json9(err);
    })


});



module.exports = router;