const mangoose = require('mongoose');
const CategorySchema = mangoose.Schema({
    _id: mangoose.Schema.Types.ObjectId,
    name: { type: String, required: true},
    pic: { type: String, required: true}
    
});

module.exports = mangoose.model('Category',CategorySchema);