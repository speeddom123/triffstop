const mangoose = require('mongoose');
const UserSchema = mangoose.Schema({
    _id: mangoose.Schema.Types.ObjectId,
    userName: { type: String, required: true},
    fname: { type: String, required: true},
    lname: { type: String, required: true},
    email: { type: String,
         required: true,
          unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
        },
    password: { type: String, required: true},
    address: { type: String, required: true},
    phoneNumber: { type: Number, required: true},
    
    
});

module.exports = mangoose.model('User',UserSchema);