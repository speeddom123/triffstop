const mangoose = require('mongoose');
const ItemSchema = mangoose.Schema({
    _id: mangoose.Schema.Types.ObjectId,
    name: { type: String, required: true},
    pic: { type: String, required: false},
    condition: { type: String, required: true},
    description: { type: String, required: true},
    returnPolicy: { type: String, required: true},
    qty: { type: Number, required: true},
    price: { type: Number, required: true},
   userId: { type: mangoose.Schema.Types.ObjectId, ref: 'User'},
    categoryId: { type: mangoose.Schema.Types.ObjectId, ref: 'Category'}
    
    
});

module.exports = mangoose.model('Item',ItemSchema);