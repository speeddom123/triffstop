const express = require('express');
const mangoose = require('mongoose');
const app = express();
const morgan = require('morgan');

const patreonRoutes = require('./api/routes/User');
const EventRoutes = require('./api/routes/Item');
const oganiserRoutes = require('./api/routes/Category');
const bodyparser =require('body-parser');
//logger

mangoose.connect('mongodb+srv://eventband:'+ process.env.MONGO_ATLAS_PW+'@eventband-jwmms.mongodb.net/<dbname>?retryWrites=true&w=majority',
{
    useNewUrlParser :true,
    useUnifiedTopology: true
})
app.use(morgan('dev'));
app.use('/uploads',express.static('uploads'))
app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
  });
app.use('/user', patreonRoutes);
app.use('/item', EventRoutes);
app.use('/category', oganiserRoutes);
//error
app.use((req,res,next) =>{
    const error = new Error('not Found');
    error.status=404;
    next(error);
});

app.use((error,req,res,next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
                error:{
                    message: error.message
                }
        }
    })
});

module.exports = app;