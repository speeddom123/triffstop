import React, { Component } from 'react';

import {Table, Row} from 'reactstrap'
import Rows from './table'

class Item extends Component {
    state = {  
        isloading : true,
        items: []
    }
    async componentDidMount(){
        const response = await fetch('/api/item');
        const body = await response.json();
        this.setState({items : body, isloading : false});
    }
    render() { 
        const {items, isloading } = this.state;
        if(isloading)
        {
            return ( <h2>  Loading... </h2>)
        }
        return ( 
            
            <div>
                
                <h2>
                    Items
                </h2>
               
               
                   
                         <Table striped bordered hover>
                         <thead>
                           <tr>
                             <th>#</th>
                             <th>First Name</th>
                             <th>Last Name</th>
                             <th>Username</th>
                           </tr>
                         </thead>
                         <tbody>
                           
                               <Rows item = {items}/>
                                   
                             
                           
                           </tbody>
                           </Table>
                    )}
            </div>
         );
    }
}

 
export default Item;