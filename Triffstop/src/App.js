
import React from 'react';
import logo from './logo.svg';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './Cotainer/Home';
import { Hero } from './Component/Hero';
import Body from './Component/Body';
import Item from './Cotainer/Item';
import SignUp from './Cotainer/Signup';
import  './App.css'
import Category from './Cotainer/Category';
import AddItem from './Cotainer/AddItems';
import Navbar from './Component/navbar';
import NavBarComponent from './Component/navbarcomp/NavBarComponent';
import SignIN from './Cotainer/Sign in';
import Footer from './Component/footer/footer';
import Profile from './Cotainer/Profile/profile';
import ProfileEdit from './Cotainer/Profile/edituse';
import Contacts from './Cotainer/Profile/owned';
import ContactSeller from './Cotainer/Seller/contacSeller';
import OtherItems from './Cotainer/Seller/other Items';
import Rating from './Cotainer/Seller/cmments';
import { createBrowserHistory } from 'history';

function App() {
  const history = createBrowserHistory();
  return (
   

   
   <Router  history={history}>

      <div className="App" >
      <Navbar></Navbar>
        
               
                
                

                <Route path="/" exact component = {Home}></Route>
                <Route path="/Item/:id" component = {Item}></Route>
                <Route path="/sign-up" component = {SignUp}></Route>
                <Route path="/sign-in" component = {SignIN}></Route>
                <Route path="/Category/:name/:id" component = {Category}></Route>
                <Route path="/addItem/:id" component = {AddItem}></Route>
                <Route path="/User/Profile" component = {Profile}></Route>
                <Route path="/User/edit" component = {ProfileEdit}></Route>
                <Route path="/User/contacts" component = {Contacts}></Route>
                <Route path="/User/Seller/:id" component = {ContactSeller}></Route>
                <Route path="/User/Seller-items" component = {OtherItems}></Route>
                <Route path="/User/Seller-rating" component = {Rating}></Route>
                


            
        
        <Footer></Footer>
                  
                
               
          
          </div>


   </Router>
  
  );
}

export default App;
