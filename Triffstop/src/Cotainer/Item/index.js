import React, {  Component,useState, useEffect } from 'react'

import Navbar from '../../Component/navbar'
import { Hero } from '../../Component/Hero'
import Body from '../../Component/Body'
import "./style.css";
import { Link } from 'react-router-dom';
import axios from 'axios';
import {Container,Row,Col, Image} from 'react-bootstrap'
class Item extends Component {
  constructor(props) {
    super(props);
    console.dir(props);
    this.state = {  
            isloading : true,
            item: {
              image: 'galaxy.jpg'
           
              
            },
            seller:{
              name:'sss'
            },
            id: props.location.id,
        }
      }
        async componentDidMount(){
          console.log(this.props.match.params.id);
            const response = await axios.get(`/item/${this.props.match.params.id}`);
            
            const body = await response.data;
            this.setState({item : body, isloading : false});
            console.log(this.state.item);
            
            const sellresponse = await axios.get(`/user/${this.state.item.userId}`);
            const sellbody = await sellresponse.data;
            this.setState({seller : sellbody, isloading : false});
            console.log(this.state.seller);
            
        }
      render() { 
          const {item, isloading } = this.state;
          
          if(isloading)
          {
              return ( <h2>  Loading... </h2>)
          }
        }

      setUser(){
        let r = this.state.seller;
        console.log(r);
        sessionStorage.setItem('seller',JSON.stringify(r) )
        let data = sessionStorage.getItem('seller');
        data= JSON.parse(data);
        console.log(data);
      }
      

  render() { 
    const pic = 'galaxy.jpg';
    
    console.log(pic);
    console.log(this.state.seller._id);
    if(pic === this.state.item.image )
    {
      console.log("same");
    }
    
    
   
    
    return ( 
      <Container>
             <Row className=''>
                <div style={{color: "black", fontWeight: 700, boxShadow:"0px 0px 0px -18px #000000", fontSize:"50px",
        textShadow: "2px 2px grey", paddingLeft:"10px", width: "100%" , marginBottom:"50px"}}> Window shopping</div>
                     
                </Row>   
             <Row>
                  <Col lg={8}>
                  
                  <div >

                            
                       <img className="itemPicture"  src= {`/uploads/${this.state.item.pic}`} alt = "image" />

                    </div>
                  </Col>
                  <Col className="infoItem" lg={3} fluid>
                  
                  
                    <Row className="sellerInfo">
                          <Container>
                             
                              <Row>
                                <Col>
                                <Row>
                                      <a className="sellerTitle">Seller Info</a>
                              </Row>
                              <Row>
                                      <a className="">{this.state.seller.fname} {this.state.seller.lname} </a>
                                      
                                      </Row>
                                </Col>
                                 <Col>
                                 <Row>
                                  <div></div>
                                      
                                      </Row>
                                      <Row>
                                      <a>Rating :</a>  <a>***  </a>
                                      
                                      </Row>
                                   
                                      
                                 </Col>
                                
                                    
                                </Row>
                                     
                                  
                                
                          </Container>
                              


                    </Row>
                    <Row   >
                                <div className="itemmName">
                                    <a>{this.state.item.name}</a>
                                </div>
                      </Row>   
                      <Row>
                        <Container>
                        <Row className="Price">
                                    <div>
                                                <a>Price:</a> <div className="Prices"><a >$JMD {this.state.item.price}</a> </div>
                                            </div>
                                  
                                </Row>

                                <Row className="Price">
                                          <div >
                                                      <a>Condition :</a> <div className="infos"><a >{this.state.item.condition}</a></div>
                                                  </div>
                                  
                                  </Row>
                                  <Row className="Price">
                                          <div >
                                                  <a>Quantity :</a> <div className="infos"><a>{this.state.item.qty}</a></div>
                                                  </div>
                              
                                                              
                                  </Row>
                        </Container>
                                
                                
                                  
                                  
                      </Row>  
                      <Row>
                                <div className="SellerLocation">
                                                <a  >Seller Location</a>
                                                <div className="infos"> <a>{this.state.seller.address}</a> </div>
                                          
                                              <div className="ContactSeller">
                                              <Link to={{ pathname:`/User/Seller/${this.state.seller.name}` , seller : this.state.seller,item:this.state.item }} onClick={ () => this.setUser()}>  <a>Contact Seller</a></Link> 
                                              
                                              </div>
                                          </div>
                            
                      </Row>  
                       
                               
                                
                              
                  </Col>  
                             

                          

                     


             </Row>

              <Row className='descriptionInfoCon'>
                <div style={{color: "white", fontWeight: 700, boxShadow:"0px 19px 37px -18px #000000", fontSize:"50px",
        textShadow: "4px 4px grey", backgroundColor: "#FF5F28", paddingLeft:"3%",paddingRight:"10px", width: "100%"}}> Description</div>
                      <div className=''  style={{ paddingLeft:"10px", width: "100%" ,border: "black solid 2px",marginTop: "3%",marginLeft:'5%',marginRight:'20%',
    marginBottom: "10%",
    padding: "40px" }} >
                            <a>{this.state.item.description}</a>
                          </div>
                </Row>   
                  


              

          


      </Container>
      
 
     );
  }
}
 
export default Item;



