
import React, { Component } from 'react';
import "./style.css";
import { Form } from 'reactstrap';
import Card from '../../Component/UI/Card';
import Body from '../../Component/Body';
import SideBar from '../../Component/sidebar';
import axios from 'axios';



class AddItem extends Component {
    constructor(props) {
        super(props);
        this.state = {  
           
            name: "",
            description: "",
            image: "galaxy.jpg",
            condition: "",
            returnPolicy: "",
            qty: 0,
            price: 0,
            user : 1,
            category : 2,
            user:
                    props.location.user ,
            category :{ 
                id:2
            }
        }
       
    }
    changeHandler = e =>{
        this.setState({[e.target.name] : e.target.value})

        console.log(this.props.location.user) ;
    }
    submitHanler = e => {
        
        e.preventDefault();
        console.log(this.state);
      
        axios.post('/api/item/', this.state)
        .then( Response => {
            console.log(Response)
        }).catch(error =>{
            console.log(error)
        })
}
    render() { 
        const {name,description,image,condition,returnPolicy,qty,price,user,category} = this.state;
        return ( 
            <div className="addcon">
                <div>

                    <Form  className="addform" onSubmit={this.submitHanler} >
                        AddItem
                        <div>
                                <input name="name"  value={name} placeholder="Item Title" onChange={this.changeHandler}></input>
                        </div>
                        <div>
                            <a>Picture of Item</a>
                            <input type='file'  onChange={this.changeHandler}></input>
                        </div>
                         <div>
                            <a>Condition : <select name="condition" placeholder="Condition" value={condition} onChange={this.changeHandler}><option>New</option> <option>Used</option></select></a>
                        </div>    
                        <div>
                            <a>Description :</a>
                            <textarea name="description" value={description} placeholder="Describ Item" onChange={this.changeHandler}></textarea>
                        </div>
                        <div>
                            <a>Quantity : </a><input name="qty" value={qty} placeholder="quantity" onChange={this.changeHandler} ></input>
                        </div>
                        <div>
                            <a>Price : </a><input name="price" value={price} placeholder="Price" onChange={this.changeHandler}></input>
                        </div>
                        <div>
                            <a>Return Policy : <select name="returnPolicy" placeholder="Return Policy" value={returnPolicy} onChange={this.changeHandler}><option>New</option> <option>Used</option></select></a>
                        </div>  
                        <button type="submit"></button>
                    </Form>
                </div>





            </div>
         );
    }
}
 
export default AddItem;