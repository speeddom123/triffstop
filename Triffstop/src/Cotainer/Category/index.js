
import React, { Component } from 'react';
import "./style.css";

import Card from '../../Component/UI/Card';
import Body from '../../Component/Body';
import SideBar from '../../Component/sidebar';
import Pagination from '../../Component/Pagination/Pagination';
import {Container,Row,Col, Image,  Jumbotron} from 'react-bootstrap'

class Category extends Component {
    state = {  
        isloading : true,
        items: [],
        color:'',
        cat:{
            pic:'car.jpg'
        },
        currentPage : 1
       
    }
    color = {}

    async componentDidMount(){
      
        const responseCat = await fetch(`/category/${this.props.match.params.id}`);
        const category =await responseCat.json();
        this.setState({cat : category});
        console.log(this.state.cat)
        const response = await fetch(`/item/category/items/${this.state.cat._id}`);
        const body = await response.json();
        this.setState({items : body, isloading : false});
      
       
       
        this.color = this.props.location.color;
        console.log( this.color);
        if(!this.props.location.color){
            console.log("working")
            let data = sessionStorage.getItem('color');
            data= JSON.parse(data);
      
           
             this.setState({color: data})
            console.log(this.state.color );
        }else{
            sessionStorage.setItem('color',JSON.stringify(this.props.location.color) )

            let data = sessionStorage.getItem('color');
            data= JSON.parse(data);
      
           
             this.setState({color: data})
            console.log(this.state.color );
        }
        // if(!this.props.location.color)
        // {
        //     let data = sessionStorage.getItem('color');
        //     data = JSON.parse(data);
        //     this.color = data
        //     console.log(data);
        //     console.log('test');
        // }else{
        //     this.color = this.props.location.color
        // }
       
        
        
    }
     
        
    
    render() { 
      const  otherItems = {
            color: "white", fontWeight: 700, fontSize:"50px",
            textShadow: "4px 4px grey", paddingLeft:"10px", width: "100%", marginBottom:"3%",backgroundColor: `${this.state.color}`,boxShadow:"0px 19px 37px -18px #000000"
        }
        console.log(this.props.location.color)
        const {items, isloading } = this.state;
        
        const itemPerPage = 12;
        const indexOfLastPost = this.state.currentPage * itemPerPage;
        const indexOfFirstPost = indexOfLastPost - itemPerPage;
        const currentPost = this.state.items.slice( indexOfFirstPost, indexOfLastPost);
        const paginate = number => {
            console.log(number)
            this.setState({currentPage : number})
            console.log(this.state.currentPage);
        }
        //  var bg=require(`../../Assets/images/bnner/${this.state.cat.pic}`)
        var bg= require(`../../Assets/images/bnner/car.jpg`)
        let pic =  {
           
            backgroundRepeat: "no-repeat",
            backgroundImage: ` url('/uploads/${this.state.cat.pic}')`,
          };
        return ( 
           <Container  >
                <Row  >
                <div style={otherItems }><a>OUR PRODUCTS</a></div>
                </Row>
               <Row  >
              
               <Jumbotron className="jumbo-Cat" style={pic}>
                    <h1>{this.state.cat.name}</h1>
                    <p>
                        This is a simple hero unit, a simple jumbotron-style component for calling
                        extra attention to featured content or information.
                    </p>
                    {/* <p>
                        <Button variant="primary">Learn more</Button>
                    </p> */}
                    </Jumbotron>
                        
               </Row>
                
                  
                   
               <Row >

                {/* <Col lg={2} className="cats" style={{backgroundColor: `${this.state.color}`,filter: `drop-shadow(-1px 5px 4px ${this.state.color})`,marginBottom:"10%"}}>
                  
                        
                </Col> */}
                <Col> {/* <SideBar color = { this.state.color}></SideBar>  */}
                      
                <div className="">
                    
                        <div>
                        <div className="catcopg">
                                    {currentPost.map(item=>{
                                        // if( item.categoryId == this.props.match.params.id)
                                        return(
                                            <div  className=""> <Card item ={item} color = {this.state.color} ></Card> </div>
                                            
                                                )

                                                        
                                    })}
                                    </div>
                                    <Pagination postsperpage = {itemPerPage} 
                            totalPosts = {this.state.items.length} 
                            paginate = {paginate}></Pagination>
                        </div>
                            </div> 
               
               </Col>
               
                
                
                
                
           
              
                    
            
                      </Row>
             </Container>
         );
    }
}
 
export default Category;