import React, { Component } from 'react';

import "./style.css";
import Card from '../../Component/UI/Card';
import Cube from '../../Component/UI/CUbe/CarouselSample';
import Body from '../../Component/Body';
import SideBar from '../../Component/sidebar';
import { NavLink } from 'react-router-dom';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import CatCarddisplay from '../../Component/Card';
import Categoryddisplay from '../../Component/sidebar copy';
import {Form,Row,Col, Button, Jumbotron,Tabs,Tab,Carousel} from 'react-bootstrap'
const photos =
[
    {
        name: 'Photo 1',
        url:  require('../../Assets/images/recents/carim.jpg'),
    },
    {
        name: 'Photo 2',
        url: require('../../Assets/images/recents/computer.jfif')
    },
    {
        name: 'Photo 3',
        url:  require('../../Assets/images/recents/car.jfif')
    }
    
]

class Home extends Component {
    state = {  
        isloading : true,
        items: [],
        category: [],
       width  : window.innerWidth,
        height : window.innerHeight
    }
    otherItems = {
        color: "white", fontWeight: 700, boxShadow:"0px 19px 37px -18px #000000", fontSize:"50px",
        textShadow: "4px 4px grey", backgroundColor: "black", paddingLeft:"10px",paddingBottom:"10px"
    }
    firstpics = {
        backgroundColor:" rgba(147, 0, 245, 0.938)",
        color:" rgba(147, 0, 245, 0.938)"

    }
    secpic = {
        backgroundColor: "rgba(255, 149, 117)",
        color: "rgba(255, 149, 117)"
    }
    thrdpic = {
        backgroundColor: "#14AE88",
        color: "#14AE88"
    }
    fthpic = {
        backgroundColor: "#F5688A",
        color: "#F5688A"
    }
    handleResize = () => {
      this.setState({width: window.innerWidth});
  }

  
  
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }
    async componentDidMount(){
        window.addEventListener("resize", this.handleResize);
        const response = await fetch('/item');
        
        console.log(response);
        const body = await response.json();
        this.setState({items : body, isloading : false});
        console.log(body);

        const Catresponse = await fetch('/category');
        const cat = await Catresponse.json();
        this.setState({category : cat, isloading : false});
        console.log(this.state.category);
        
    }
    
    render() { 
        const settings = {
            dots: true,
            fade: true,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            arrows: true,
            slidesToScroll: 1,
            className: "slides",
            autoplay: true,
            autoplaySpeed: 3000
          };
        const {items, isloading } = this.state;
        console.log(items);
        if(isloading)
        {
            return ( <h2>  Loading... </h2>)
        }
        return ( 
            <div>
            <div className = "home">
               
               <div className="recent">
               <div className="hero">
              
              <div>
                
                <div
              
                >{this.state.width > 550 &&    <Cube pic = {'../../Assets/images/recents/display.jpg'} />
                }
           
         
         
                </div>
              
               
            </div>
               </div>
               
               
                        <div className="firstpics" >
                        <CatCarddisplay item={this.state.category[0]} color= {this.firstpics}/>

                        </div>
                        <div className="secpic">
                        <CatCarddisplay item={this.state.category[1]} color= {this.secpic}/>
                            </div>
                            <div className="thrdpic">
                            <CatCarddisplay item={this.state.category[0]} color= {this.thrdpic}/>
                            </div>
                            <div className="fthpic">
                            <CatCarddisplay item={this.state.category[0]} color= {this.fthpic}/>
                            </div>
               
               </div>
             
               <div className="infoItems">
                  
                   <div className="infoHead">
                   <div style={this.otherItems }><a>OUR PRODUCTS</a></div>
                   </div>
                   <div className="infoBody" >
                   <Tabs className="myClass" defaultActiveKey="home" id="uncontrolled-tab-example">
                    <Tab  eventKey="home" style={{ padding:'2%'}} title="Best Seller">
                    <div className="itemCoon">
                            {items.slice(0,7).map(item=>{ 
                                 console.log(item)
                                return(
                                   
                                        <div  > <Card item ={item} color={"#FF5F28"} ></Card> </div>
                                        
                                        )
    
                                                
                            })}
                            
                          </div>
                    </Tab>
                    <Tab eventKey="profile" style={{ padding:'2%'}} title="Newest">
                  
                                  <div className="itemCoon">
                            {items.slice(0,10).map(item=>{ 
                                 console.log(item)
                                return(
                                   
                                        <div  > <Card item ={item} color={"#FF5F28"} ></Card> </div>
                                        
                                        )
    
                                                
                            })}
                            
                          </div>
                    </Tab>
             
                    </Tabs>
                   </div>
                   
               </div>
               
              
                   
                   
                      </div>
                      

                      
                      
                      
                      
                      </div>

                      
            
         );
    }
}

 
export default Home;
