
import React, { Component } from 'react'
import axios from 'axios';
import "./style.css";
import { Form } from 'reactstrap';
import { Link } from 'react-router-dom';


class ProfileEdit extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            user: {}
         }
    }
    async componentDidMount(){
            
              
        let data = sessionStorage.getItem('user');
        data= JSON.parse(data);
        console.log(data);
         
          
          if(data)
          {
            this.setState({user : data});
            console.log(this.state.user);
          }
          
         
      }
    render() { 
        const {name,lname,email,password,id,adress} = this.state;
        return ( 
            <div className="profile">
               <div className="info">
                   <div className="picCon"><img className="ProPic" src= {require('../../Assets/icons/User.svg')} alt = "image"  /> </div>
                   <div>
                       <div className="userInfoBox">
                          <a>First Name:   {this.state.user.fname} </a>  
                          <a>Last Name:  {this.state.user.lname} </a>  
                          <a>Adress:  {this.state.user.address}</a>  
                          <a>Email:  {this.state.user.email}</a>  
                          <a>Phone Number:  {this.state.user.phoneNumber}</a> 
                          
                          <Link to='/User/edit'><a className="edit">Edit</a></Link> 


                       </div>
                   </div>
                </div>
                <div className="sideoptions">
                <Link to='/User/Profile'> <a>Item Owned </a></Link> 
                    <a>WishList </a> 
                    <a>Saved </a> 
                    <a>Selling </a> 
                    <div>
                        <a>Guides </a> 
                        <a>Buying </a> 
                        <a>Selling </a> 
                    </div>
                </div>
                <div className="MainInfo">
                <Form className="edit-form" >
                                <a>First Name</a>
                                <input className="edit-up" placeholder="First Name" name="name" value={name} onChange= {this.changeHandler}></input>
                                <a>Last Name</a>
                                <input className="edit-up" placeholder="Last Name" name="lname" value={lname} onChange= {this.changeHandler}/>
                                <a>Email</a> 
                                <input className="edit-up" placeholder="Email" name="email" value={email} onChange= {this.changeHandler}/>
                                 <a>Password</a> 
                                 <input className="edit-up" placeholder="Password" name="password" value={password} onChange= {this.changeHandler}/>
                                 <a>Phone</a>
                                 <input className="edit-up" placeholder="Phone Number" name="id" value={id} onChange= {this.changeHandler}/>
                                <a>Adress</a>
                                <input className="edit-up" placeholder="Address" name="adress" value={adress} onChange= {this.changeHandler}/>
                                
                               
                               


                        </Form>
                        <img className="subarrow" src= {require('../../Assets/icons/submits.svg')} alt = "image"  onClick={this.submitHanler}/> 
                </div>
            </div>
         );
    }
}
 
export default ProfileEdit;