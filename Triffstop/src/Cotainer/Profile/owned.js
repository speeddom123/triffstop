

import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import axios from 'axios';
import "./style.css";
import { Form } from 'reactstrap';
import { Link } from 'react-router-dom';
import Template from "../../Component/UI/contacttemplate/ContactTemplate"
import {Container,Row,Col, Image, Button} from 'react-bootstrap'

const Contacts =(props)=>  {
   
    const dispatch = useDispatch();
    const [Items, setItems] = useState([]);
    const [addItems, setaddItems] = useState(false);
    const [edititems, setedititems] = useState(false);
    const [ItemEdit, setItemEdit] = useState({});
    const [contacts, setcontacts] = useState(0);
    const user = useSelector((state) => state.appreducer.user);
    useEffect(async () => {
      console.log("test");
      console.log(user);
      await axios .get(`/item/user/items/${user._id}`)
        .then((response) => {
          setItems(response.data);
        
        })
  
        await axios.get( `/contact/`,{
              
          headers: {
            'Authorization': `token ${user.token}`,
            'Content-Type': 'multipart/form-data'
          } },
             
           ).then( Response => {
  
          console.log(Response.data)
       
          setcontacts(Response.data);
      }).catch(error =>{
          console.log(error)
      })
        
      
      
    }, []);
   
        

     
        console.log(contacts);
        console.log(this.message);


        return ( 
            <Container>
                <Row className="sellerInformation">
                                <Col lg="3">
                            <div className=""><img className="ProPic" src= {require('../../Assets/icons/User.svg')} alt = "image"  /> </div>
                    </Col>
                    <Col>
                            <div className="userInfoBox">
                                        <a>First Name:   {user.fname} </a>  
                                        <a>Last Name:  {user.lname} </a>  
                                        <a>Adress:  {user.address}</a>  
                                        <a>Email:  {user.email}</a>  
                                        <a>Phone Number:  {user.phoneNumber}</a> 
                                        
                                        <Link to='/User/edit'><a className="edit">Edit</a></Link> 


                                    </div>
                    </Col>
                </Row>
                
                        
                <Row>
                        <Col lg="2">
                        <div className="sideoptions">
                            <Link to='/User/Profile'> <a>Item Owned </a></Link> 
                                
                            <Link to='/User/contacts'> <a>Contacts ({contacts})</a></Link> 
                                <a>Saved </a> 
                                <a>Selling </a> 
                                <div>
                                    <a>Guides </a> 
                                    <a>Buying </a> 
                                    <a>Selling </a> 
                                </div>
                            </div>
                        </Col>
                        <Col>
                                
                            <div className="MainInfo">
                            {ItemEdit.map(contact=>{ 
                            return(
                                    <div className="contact" >    <Template contact = {contact}></Template> </div>
                                    
                                    )

                                            
                        })}
                      
                            </div>
                        </Col>
                        
              
                </Row>
               
            </Container>
          
         );
    }

 
export default Contacts;