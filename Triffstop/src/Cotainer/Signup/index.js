import React, { Component } from 'react'
import axios from 'axios';
import "./style.css";
import Map from '../../Component/Map/index';
import { createBrowserHistory } from 'history';
import { Link } from 'react-router-dom';
import {Container,Row, Button, Col} from 'react-bootstrap'
import Form from 'react-bootstrap/Form'

class SignUp  extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            fname: '',
            
            userName : '',
            lname : '',
            email : '',
            password : '',
            phoneNumber : '',
            address : '',
            

        }
    }
    componentDidMount(){
       const history = createBrowserHistory();
       
         
       
    }
    changeHandler = e =>{
        this.setState({[e.target.name] : e.target.value})
    }
    submitHanler = e => {
        console.log('click')
        
            console.log(this.state);
          
            axios.post('/user/signup', this.state)
            .then( Response => {
                //console.log(Response.data)
                sessionStorage.setItem('user',JSON.stringify(Response.data) )
                let data = sessionStorage.getItem('user');
                data= JSON.parse(data);
                console.log(data);
                console.log('test');
                 return this.props.history.push('/')
            }).catch(error =>{
                console.log(error)
            
            })
           
   
    }
    render() { 
        const {fname,userName,lname,email,password,phoneNumber,address} = this.state;
        
        return ( 

                        <div className="signUserContainer">
                            
                        {/* <div className="signUserContainer">
                        <div className="signUpTitle">
                                <a >Create an Account</a>
                        </div>
                        <Form className="signADDUP" >
                                <div className="fname"><a>First Name</a><input className="sign-up" placeholder="First Name" name="fname" value={fname} onChange= {this.changeHandler}></input></div>
                                <div className="lname"> <a>Last Name</a><input  className="sign-up" placeholder="Last Name" name="lname" value={lname} onChange= {this.changeHandler}/></div>
                                <div className="userName"><a>User Name</a><input className="sign-up" placeholder="User Name" name="userName" value={userName} onChange= {this.changeHandler}></input></div>
                                <div className="Email"><a>Email</a> <input className="sign-up" placeholder="Email" name="email" value={email} onChange= {this.changeHandler}/></div>
                                <div className="Pass"> <a>Password</a> <input className="sign-up" placeholder="Password" name="password" value={password} onChange= {this.changeHandler}/></div>
                                <div className="phoneNumber"> <a>Phone</a><input className="sign-up" placeholder="Phone Number" name="phoneNumber" value={phoneNumber} onChange= {this.changeHandler}/></div>
                                <div className="Adress"> <a>Adress</a><input className="sign-up" placeholder="Address" name="address" value={address} onChange= {this.changeHandler}/></div>
                                
                               
                               


                        </Form>
                        
                       <img className="subarrow" src= {require('../../Assets/icons/submits.svg')} alt = "image"  onClick={this.submitHanler}/> 
                        </div> */}
                        <Container>
                        <Row  >
                            <Col  className="signupImage d-none d-lg-block">
                           
                            </Col>
                            <Col>
                            
                        <Form className="">
                            <Form.Group className="signUpTitle" controlId="formBasicPassword">      
                                        <Form.Label className="text-dark" ><h2>Sign Up</h2></Form.Label>
                                        
                             </Form.Group>
                        
                            <Row>

                                <Col  xs="12" sm="12" md ="12">
                                     <Form.Group controlId="formBasicPassword">      
                                        <Form.Label column="lg">First Name:</Form.Label>
                                        <Form.Control  className="sign-up" type="text" placeholder="First Name"  name="fname" value={fname} onChange= {this.changeHandler} />
                                    </Form.Group>
                                </Col>
                                <Col  xs="12" sm="12" md ="12">
                                        <Form.Group controlId="formBasicPassword">
                                        <Form.Label column="lg">Last Name</Form.Label>
                                        <Form.Control  className="sign-up" type="text" placeholder="Last Name" name="lname" value={lname} onChange= {this.changeHandler} />
                                    </Form.Group>
                                </Col>
                            </Row>
                            
                           <Row>
                                    <Col  xs="12" sm="12" md ="12">
                                                <Form.Group controlId="formBasicPassword">
                                            <Form.Label column="lg">User Name</Form.Label>
                                            <Form.Control  className="sign-up" type="text" placeholder="User Name"  name="userName" value={userName} onChange= {this.changeHandler} />
                                        </Form.Group>
                                    </Col>
                                    <Col  xs="12" sm="12" md ="12">
                                                <Form.Group controlId="formBasicPassword">
                                            <Form.Label column="lg">Phone Number</Form.Label>
                                            <Form.Control  className="sign-up" type="text" placeholder="1876-XXX-XXXX"  name="phoneNumber" value={phoneNumber} onChange= {this.changeHandler} />
                                        </Form.Group>
                                    </Col>
                           </Row>
                           <Row>
                                <Col>

                                    </Col>
                                    <Col>

                                    </Col>
                           </Row>
                           <Row>
                                  <Col  xs="12" sm="12" md ="12">
                                            <Form.Group controlId="formBasicPassword">
                                            <Form.Label column="lg">Password</Form.Label>
                                            <Form.Control  className="sign-up" type="password" placeholder="Password" name="password" value={password} onChange= {this.changeHandler} />
                                        </Form.Group>
                                    </Col>
                                    <Col  xs="12" sm="12" md ="12">
                                                <Form.Group controlId="formBasicEmail">
                                            <Form.Label column="lg">Email address</Form.Label>
                                            <Form.Control  className="sign-up" type="email" placeholder="XXX@xx.com" name="email" value={email} onChange= {this.changeHandler} />
                                            <Form.Text className="text-muted ">
                                            We'll never share your email with anyone else.
                                            </Form.Text>
                                        </Form.Group>
                                    </Col>
                           </Row>
                            
                           
                            
                           

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label column="lg">Address</Form.Label>
                                <Form.Control  className="sign-up" type="text" placeholder="Address" name="address" value={address} onChange= {this.changeHandler} />
                            </Form.Group>
                            {/* <Map
                                google={this.props.google}
                                center={{lat: 18.5204, lng: 73.8567}}
                                height='300px'
                                zoom={15}
                                /> */}
                            <Form.Group controlId="formBasicCheckbox">
                                <Form.Check type="checkbox" label="Check me out" />
                            </Form.Group>
                            <Button className="buttonSignUp"  onClick={ () => this.submitHanler()} variant="dark" >
                                Submit
                            </Button>
                            </Form></Col>
                            </Row>
                            </Container>
                        </div>
                        

         );
    }
}
 
export default SignUp ;