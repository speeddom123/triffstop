import React, { useEffect,useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import "./style.css";
import pic from "../../Assets/images/background/SignIn.jpg"
import { Link } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import {Form,Row,Col, Button, Container, Image} from 'react-bootstrap'
import login from '../../store/actions/login'
const SignIN =(props)=>  {
const [email,setemail]= useState('')
const [password,setpassword]= useState('')
const dispatch = useDispatch();
    useEffect(() => {
        const history = createBrowserHistory();
      }, []);
   
    const emailHandler = e =>{
      setemail(e.target.value)
    }
    const passHandler = e =>{
        setpassword(e.target.value)
      }
 const   submitHanler = () => {
        
           
            console.log(email,password);
          const user = {email,password}
            axios.post('/user/login',user)
            .then( Response => {
                console.log(Response.data)
                let s = Response.data.user;
                console.log(Response)
                let userdata = Response.data.user[0];
                userdata.token =Response.data.token
                dispatch(login(userdata))
                setUser(userdata)
              //  console.log(data);
                return props.history.push('/User/Profile')
            }).catch(error =>{
                console.log(error)
            })
            
    }
    const setUser =(user) =>{
        
       
        sessionStorage.setItem('user',JSON.stringify(user) )
        let data = sessionStorage.getItem('user');
        data= JSON.parse(data);
        console.log(data);
        console.log('test');
      }

        return ( 
                        
                        // <Container   >
                        //     <Row className="justify-content-center">
                        //         <Col  className="justify-content-center" >
                        //              <div className="logInUserContainers" > 
                        //         <Row className="justify-content-center">
                        //                    <div className="Emails">
                        //                     <a>Sign In</a>
                        //                        </div> 
                        //         </Row>
                        //         <Row className="justify-content-center">
                        //              <Form className="user_In"  >
                        //                     <Container>
                        //                         <Col>
                                               
                        //                         <Row >
                        //                                 <div ><a >Email</a></div>
                        //                         </Row>
                        //                         <Row >
                        //                                 <div > <input className="sign" placeholder="Email" name="email" value={email} onChange= {this.changeHandler}/></div>
                        //                         </Row>
                        //                         <Row >
                        //                                  <div > <a>Password</a> </div>
                        //                         </Row>
                        //                         <Row >
                        //                                  <div ><input className="sign"  placeholder="Password" name="password" value={password} onChange= {this.changeHandler}/></div>
                        //                         </Row>
                        //                         <Row  >
                                                 

                        //                            <div className="justify-content-center" className="signInButton">
                        //                                      <Button  className="justify-content-center" onClick={ () => this.submitHanler()} variant="dark">Sign In</Button>
                                                             
                        //                            </div>
                                                
                                                   
                        //                         </Row>
                        //                         </Col>
                        //                         </Container>
                        //             </Form>
                        //         </Row>
                        //            </div> 
                        //         </Col>
                        //         </Row>
                        // </Container>
                        <div className="logInUserContainers">

                      
                        <Container>
                            <Row>
                                <Col   lg="5"   className="signImage d-none d-lg-block">
                                        
                                  
                                </Col>
                                <Col>
                                </Col>
                                <Col  xs="12" lg="5">
                                <Form className="signInform">
                            
                                    <Form.Group className="signUpTitle" controlId="formBasicPassword">      
                                                <Form.Label className="text-dark" column="lg"><h2>Sign In</h2></Form.Label>
                                                
                                    </Form.Group>
                           
                            <Row className="">
                                <Col xs = "12">
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control className="sign-up" type="email" placeholder="Enter email" name="email" value={email} onChange= {emailHandler}/>
                                        <Form.Text  className="text-muted ">
                                        We'll never share your email with anyone else.
                                        </Form.Text>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control  className="sign-up" type="password" placeholder="Password" name="password" value={password} onChange= {passHandler}/>
                                    </Form.Group>
                                   
                                    <Button  className="justify-content-center" onClick={ () => submitHanler()} variant="dark">Sign In</Button>
                                
                                </Col>
                                </Row>
                          
                           
                        </Form>
                        
                                </Col>
                                <Col>
                                </Col>
                            </Row>
                        </Container>
                        
                        </div>
                                
                           
                               
                                
                                
                               
                               

                        
                       
         );
    
}
 
export default SignIN ;