
import React, { Component } from 'react'
import axios from 'axios';
import "./style.css";
import { Form } from 'reactstrap';
import { Link } from 'react-router-dom';


class Rating extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            seller:{
                name:""
            },
           id:this.props.match.params.id
         }
        }
    async componentDidMount(){
            
              
        let data = sessionStorage.getItem('seller');
        data= JSON.parse(data);
        console.log(data);
          this.setState({seller : data});
          console.log(this.state.seller);
          
         
      }
    render() { 
        const {name,lname,email,password,id,adress} = this.state;
        return ( 
            <div className="profile">
                <div className="info">
                   <div className="picCon"><img className="ProPic" src= {require('../../Assets/icons/User.svg')} alt = "image"  /> </div>
                   <div>
                       <div className="userInfoBox">
                         <a>First Name: {this.state.seller.name} </a> 
                          <a>Last Name: {this.state.seller.lname}</a>  
                          <a>Adress: {this.state.seller.adress}</a>  
                          <a>Email: {this.state.seller.email}</a>  
                          <a>Phone Number: {this.state.seller.phonenum}</a> 
                          
                          <Link to=''><a className="edit">Edit</a></Link> 
                          
                          <Link to=''><a className="edit">Edit</a></Link> 


                       </div>
                   </div>
                </div>
                <div className="sideselleroptions">
                <Link to={{ pathname:`/User/Seller/${this.state.seller.name}`}}>  <a>Contact Seller</a></Link> 
                <Link to='/User/Seller-items'> <a>Other Items From Seller </a> </Link> 
                    <a>Rating </a> 
            
                </div>
                <div className="MainInfo">
                        <div className="commentCon">
                            <a>Seller Name:</a>
                           <div>
                                 <a>Ratings:</a> <a>***</a>
                               </div>
                               <a>Comments</a>

                        </div>
                </div>
            </div>
         );
    }
}
 
export default Rating;