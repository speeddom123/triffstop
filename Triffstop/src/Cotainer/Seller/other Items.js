
import React, { Component } from 'react'
import axios from 'axios';
import "./style.css";
import { Form } from 'reactstrap';
import { Link } from 'react-router-dom';
import Card from '../../Component/UI/Card';


class OtherItems extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            seller:{
                name:""
            },
            items:[],
           id:this.props.match.params.id
         }
        }
    async componentDidMount(){
          
        let data = sessionStorage.getItem('seller');
        data= JSON.parse(data);
        console.log(data);
          this.setState({seller : data});
          this.state.seller = data
          console.log(this.state.seller);
          axios.get(`/item/user/items/${this.state.seller._id}`)
          .then( Response => {
              //console.log(Response.data)
             
              console.log(Response.data);
              //this.state.Items =Response.data;
              this.setState({items : Response.data});
              console.log( this.state.Items);
               
          }).catch(error =>{
              console.log(error)
          
          }) 
         
      }
    render() { 
       // const {items } = this.state.items;
        return ( 
            <div className="profile">
                <div className="info">
                   <div className="picCon"><img className="ProPic" src= {require('../../Assets/icons/User.svg')} alt = "image"  /> </div>
                   <div>
                       <div className="userInfoBox">
                        <a>First Name: {this.state.seller.fname} </a> 
                          <a>Last Name: {this.state.seller.lname}</a>  
                          <a>Adress: {this.state.seller.address}</a>  
                          <a>Email: {this.state.seller.email}</a>  
                          <a>Phone Number: {this.state.seller.phoneNumber}</a> 
                          
                          
                           


                       </div>
                   </div>
                </div>
                <div className="sideselleroptions">
                <Link to={{ pathname:`/User/Seller/${this.state.seller.name}`}}>  <a>Contact Seller</a></Link> 
                    <a>Other Items From Seller___ </a> 
                    <Link to='/User/Seller-rating'>  <a>Rating </a> </Link> 
            
                </div>
                <div className="MainInfo">
                <div className="catcopg" style={{paddingBottom: "10%"}}>
                             {this.state.items.map(item=>{
                                
                                  return(
                                       <div  className="item"> <Card item ={item} ></Card> </div>
                                       
                                        )

                                                  
                              })}
                              

                      </div>
                </div>
            </div>
         );
    }
}
 
export default OtherItems;