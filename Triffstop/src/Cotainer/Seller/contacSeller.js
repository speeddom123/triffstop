
import React, { Component } from 'react'
import axios from 'axios';
import "./style.css";
import { Form } from 'reactstrap';
import { Link } from 'react-router-dom';
import ContactForm from "../../Component/Contact/ContactForm"
import {Container,Row, Button, Col} from 'react-bootstrap'

class ContactSeller extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            seller:{
                name:""
            },
           id:this.props.match.params.id
         }
        }
         async componentDidMount(){
            
              console.log(this.props.location.item)
            let data = sessionStorage.getItem('seller');
            data= JSON.parse(data);
            console.log(data);
              this.setState({seller : data});
              console.log(this.state.seller);
              
             
          }
          
    render() { 
        const {name,lname,email,password,id,adress} = this.state;
        return ( 


            <Container>
                 <Row className="sellerInformation">
                                <Col lg="3">
                            <div className=""><img className="ProPic" src= {require('../../Assets/icons/User.svg')} alt = "image"  /> </div>
                    </Col>
                    <Col>
                            <div className="userInfoBox">
                                        <a>First Name:   {this.state.seller.fname} </a>  
                                        <a>Last Name:  {this.state.seller.lname} </a>  
                                        <a>Adress:  {this.state.seller.address}</a>  
                                        <a>Email:  {this.state.seller.email}</a>  
                                        <a>Phone Number: {this.state.seller.phoneNumber}</a> 
                                        
                                        <Link to='/User/edit'><a className="edit">Edit</a></Link> 


                                    </div>
                    </Col>
                </Row>
             
                <Row>
                    <Col lg="3">
                    <div className="sideselleroptions">
                        <a>ContactSeller___ </a> 
                        <Link to='/User/Seller-items'> <a>Other Items From Seller </a> </Link> 
                        <Link to='/User/Seller-rating'>  <a>Rating </a> </Link> 
                        
                
                    </div>
                    </Col>
                    <Col >
                    <ContactForm item = {this.props.location.item}></ContactForm> 
                    </Col>
                </Row>
            </Container>
            // <div className="profile">
            //     <div className="info">
            //        <div className="picCon"><img className="ProPic" src= {require('../../Assets/icons/User.svg')} alt = "image"  /> </div>
            //        <div>
            //            <div className="userInfoBox">
            //               <a>First Name: {this.state.seller.fname} </a> 
            //               <a>Last Name: {this.state.seller.lname}</a>  
            //               <a>Adress: {this.state.seller.address}</a>  
            //               <a>Email: {this.state.seller.email}</a>  
            //               <a>Phone Number: {this.state.seller.phoneNumber}</a> 
                          
                          


            //            </div>
            //        </div>
            //     </div>
            //     </div>
            //     <div className="sideselleroptions">
            //         <a>ContactSeller___ </a> 
            //         <Link to='/User/Seller-items'> <a>Other Items From Seller </a> </Link> 
            //         <Link to='/User/Seller-rating'>  <a>Rating </a> </Link> 
                    
            
            //     </div>
            //     <div className="MainInfo">
            //     <div>
            //     <ContactForm item = {this.props.location.item}></ContactForm>
            // </div> 
                       
            //     </div>
            // </div>
            
          
         );
    }
}
 
export default ContactSeller;