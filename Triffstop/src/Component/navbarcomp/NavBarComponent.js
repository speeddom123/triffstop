/**
 *
 * This file was generated with Adobe XD React Exporter
 * Exporter for Adobe XD is written by: Johannes Pichler <j.pichler@webpixels.at>
 *
 **/

import React from "react";
import { NavLink } from 'react-router-dom';
import "./style.css";
const NavBarComponent = () => (
  <svg width={1520} height={125} viewBox="0 0 1520 125">
    <defs>
     
      <filter
        id="a"
        x={163}
        y={10}
        width={209}
        height={115}
        filterUnits="userSpaceOnUse"
      >
        <feOffset dx={20} dy={20} input="SourceAlpha" />
        <feGaussianBlur stdDeviation={10} result="b" />
        <feFlood floodColor="#f3f3f3" floodOpacity={0.161} />
        <feComposite operator="in" in2="b" />
        <feComposite in="SourceGraphic" />
      </filter>
    </defs>
    <g className="a">
      <rect className="f" width={1520} height={99} />
      <rect className="e" x={0.5} y={0.5} width={1519} height={98} />
    </g>
    <g transform="translate(31 26)">
      <g transform="translate(-215 -18)">
        <g className="b" transform="translate(216 24)">
          <rect className="f" width={42} height={6} />
          <rect className="e" x={0.5} y={0.5} width={41} height={5} />
        </g>
        <g className="b" transform="translate(215 37)">
          <rect className="f" width={42} height={6} />
          <rect className="e" x={0.5} y={0.5} width={41} height={5} />
        </g>
        <g className="b" transform="translate(216 50)">
          <rect className="f" width={42} height={6} />
          <rect className="e" x={0.5} y={0.5} width={41} height={5} />
        </g>
       <div></div>
        <NavLink to="/sign-in"><text className="c" transform="translate(1512 48)">
        <tspan x={0} y={0}>
            {"LOG IN"}
          </tspan>
          
        </text>
        </NavLink>
        
        <text className="c" transform="translate(216 41)">
          <tspan x={0} y={0} />
        </text>
        <NavLink to="/sign-up"><text className="c" transform="translate(1512 48)"></text>
        <text className="c" transform="translate(1391 48)">
          <tspan x={0} y={0}>
            {"SIGN UP"}
          </tspan>
        </text>
        </NavLink>
        <text className="c" transform="translate(1600 48)">
          <tspan x={0} y={0}>
            {"ORDERS"}
          </tspan>
        </text>
        <g className="b" transform="translate(648.5 28)">
          <rect className="f" width={591} height={35} rx={17.5} />
          <rect className="e" x={0.5} y={0.5} width={590} height={34} rx={17} />
        </g>
        <g className="g" transform="matrix(1, 0, 0, 1, 184, -8)">
        <NavLink to="/"><text className="d" transform="translate(174 64)">
            <tspan x={0} y={0}>
              {"TriffStop"}
            </tspan>
          </text>
          </NavLink>
        </g>
      </g>
      <g transform="translate(984.746 9.746)">
        <path className="e" d="M0,0H35.254V35.254H0Z" />
        <path
          d="M21.362,19.158H20.2l-.411-.4a9.562,9.562,0,1,0-1.028,1.028l.4.411v1.16l7.345,7.33L28.692,26.5Zm-8.814,0a6.61,6.61,0,1,1,6.61-6.61A6.6,6.6,0,0,1,12.548,19.158Z"
          transform="translate(1.407 1.407)"
        />
      </g>
    </g>
  </svg>
);

export default NavBarComponent;
