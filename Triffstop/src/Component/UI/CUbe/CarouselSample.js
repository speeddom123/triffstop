import React, { Component } from 'react';
import { Carousel } from 'react-motion-components';

export default class CarouselSample extends Component {
  state = {
    index: 0.1,
    size: 5,
    effect: '3d',
    colors: ['green', 'red', 'blue', 'yellow', 'black']
  };

 

  move = (index) => {
    
    this.setState({
      index
    });
  };



  render() {
    const defaultStyle = {
      width: 650,
      height: "70vh",
      margin: '0 auto',
     

      
 
      
    };
const text ={
 
    position: "absolute",
    bottom: "20px",
    right: "20px",
    
    color: "white",
    paddingLeft: "20px",
    paddingRight: "20px"
  
}
    const itemStyle = {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      fontSize: 30,
      fontWeight: 'bold',
      color: 'white'
    };
  const   pics = [
      "../../../Assets/images/recents/display2.jpg",
      "../../../Assets/images/recents/display3.jpg",
      "../../../Assets/images/recents/display4.jpg",
      "../../../Assets/images/recents/display5.jpg",
      "../../../Assets/images/recents/display4.jpg"
    ]
    return (
     
       
      
    
        <div
          style={{
            ...defaultStyle
          }}
        >
          <Carousel
            {...defaultStyle}
            direction={'horizontal'}
            effect={this.state.effect}
            index={this.state.index}
            // onClick={() => {}}
            
            onChange ={(index) => {
              this.move(index);
            }}
          >
            {Array.from({ length: this.state.size }, (x, i) => {
              return (
                <div
                  key={i}
                  style={{
                    ...defaultStyle,
                    ...itemStyle,
                    
                    backgroundColor: "blue"
                  }}
                >
                 
                
                  <img  style={{
           boxShadow:"0px 19px 37px -18px #000000", height: "100%"
          }}
                       
                         src={ require(`../../../Assets/images/recents/display${i}.jpg`)}
                        alt="First slide"
                        />
                        <div style ={text}>
                          Buy Now
                        </div>
                      
                        
                </div>
              );
            })}
          </Carousel>
        </div>
    
    );
  }
}