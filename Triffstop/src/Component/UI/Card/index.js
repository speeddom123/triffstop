import React, { useEffect,useState } from 'react'
import "./style.css";
import { Link } from 'react-router-dom';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

import { Button} from 'react-bootstrap'

const Card = (props) => {
    const photos =
    [
    {
        name: 'Photo 1',
        
    },
    {
        name: 'Photo 2',
       
    }
]
    const settings = {
        dots: false,
        fade: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        arrows: true,
        slidesToScroll: 1,
        className: "imageslides"
      };
    console.log(props.color);
    const id = props.item._id;
  
  // letb pic=require(`../../../triffBackend/uploads\\159572264970105-01-2020-18.31.35.jpg`)
 
  const [style, setStyle] = useState({display: 'none'});
   // console.log(pic);
   console.log(props.item.pic);
    return (
       
        <div className="cardBack" style={{ border: `2px solid ${props.color}`}} onMouseEnter={e => {
            setStyle({display: 'block',  backgroundColor:`${props.color}`,borderRadius:"20px", color:"white"});
        }}
        onMouseLeave={e => {
            setStyle({display: 'none'})
        }}>
           
           <div className="">
               
               
             
                            <div>
                                 <img  className="slidecardimg"  src= {`/uploads/${props.item.pic}`} alt = "image" />
                               
                            </div>
                   
                       
            
               
           </div>
           
         <div className="infoGrid">
         <div className="itemCatName"> <Link to={{ pathname:`/Item/${id}` , id : props.item._id }}><a>{props.item.categoryId}</a></Link>  </div>
           <div className="itemName"> <a>{props.item.name}</a>  </div>
          
           {/* <div className="itemConditionName"> <Link to={{ pathname:`/Item/${id}` , id : props.item._id }}></Link>  </div> */}
           <div className="itemPrice"> <a  style={{ color: ` ${props.color}`}}>${props.item.price}</a>  </div>
           <Button style={style} variant="warning"><Link className="link" to={{ pathname:`/Item/${id}` , id : props.item._id }}>View Item</Link></Button>
         
           </div>
          
       </div>  

   
    )
}

export default Card
