import React from 'react'
import "./style.css";
const Logo = () => {
    return (
        <div className= "logo">
            <div className="logocon">
                <img className="logoPic" src= {require('../../Assets/logo.jpg')} alt = "image" />

            </div>
            <div className="word">
                <h1>Vidstore</h1>
            </div>
            
        </div>
    )
}

export default Logo
