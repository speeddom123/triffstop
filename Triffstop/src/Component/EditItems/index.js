
import axios from 'axios';
import "./style.css";
import {useForm} from 'react-hook-form';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom/withRouter";
import React from 'react';
import {Form,Row,Col, Button, Image} from 'react-bootstrap'


import { useState,useEffect } from 'react';


export default function EditItems(props) {
    const [cat, setCat] = useState([]);
    const [picture, setPicture] = useState({});
    const [Item, setItem] = useState({});
    const {register, handleSubmit, errors} = useForm();
      useEffect(async () => {
        const result = await axios.get(
            '/category'
        );
       console.log(props.item)
        setCat(result.data);
        setPicture( `/uploads/${props.item.pic}`)
        setItem(props.item)
        
      }, []);
      
      const    PicHandler = e =>{
        
        setPicture(URL.createObjectURL(e.target.files[0])) 
        
         
          console.log( Item)
          console.log(e.target.files[0])
          console.log(e.target.files[0].name)
       }
    const    changeHandler = e =>{
        //setItem({[e.target.name] : e.target.value})
        // setItem(prevState => ({
        //       ...prevState,  [e.target.name]: e.target.value  
        //   }));
        setItem({
            ...Item,
            [e.target.name]: e.target.value
          });
        //setPicture(URL.createObjectURL(e.target.files[0])) 
        
         
          console.log( Item)
         
         // console.log(picture)
       }
    
    const OnSubmit = async () =>{
        console.log(Item);
     
        
        let datas = sessionStorage.getItem('user');
        const user= JSON.parse(datas);
        console.log(user);
       
       
        const fd = new FormData()
        let blobpic = await fetch(picture).then(r => r.blob());
        console.log(blobpic)
        fd.append('name', Item.name)
      
            fd.append('pic', blobpic, Item.pic)
            fd.append('condition', Item.condition)
            fd.append('description', Item.description)
            fd.append('returnPolicy', Item.returnPolicy)
            fd.append('qty', Item.qty)
            fd.append('price', Item.price)
            fd.append('userId', Item.userId)
            fd.append('categoryId', Item.categoryId)
            fd.get('name')
        
        // fd.append('pic', Item.pic, Item.pic.name)
        // fd.append('condition', Item.condition)
        // fd.append('description', Item.description)
        // fd.append('returnPolicy', Item.returnPolicy)
        // fd.append('qty', Item.qty)
        // fd.append('price', Item.price)
        // fd.append('userId', Item.userId)
        // fd.append('categoryId', Item.categoryId)
        // fd.get('name')
        for (var pair of fd.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }
        console.log(fd.get('name'));  
        console.log(fd.get('description'));  
        console.log(fd);  
        console.log(Item);
        
       axios.patch( `/item/${props.item._id}`,fd ,{
            onUploadProgress : progressEvent=> {
                console.log(" Upload Progress: "  + (progressEvent.loaded / progressEvent.total *100) + '%')
            }
         ,
            headers: {
              'Authorization': `token ${user.token}`,
              'Content-Type': 'multipart/form-data'
            } },
               
             ).then( Response => {
            console.log(Response.data)
           // window.location.reload(false);
            
           
        }).catch(error =>{
            console.log(error)
        })
       //window.location.href = '/huntersHub'
    //     <Redirect to="/"  />
    //    <Link className="link" to='/huntersHub'/>*/
    }
    
    return (
        

                            // <div className="log-con">
                            // <div className="logInUserContainer">
                            // <div className="signUpTitle">
                            //         <a >Edit ITEMS</a>
                            // </div>
                            // <form className="addItem"  onSubmit={handleSubmit(OnSubmit)} >
                                    
                            //         <div className="Names"><a>Name</a> <input className="sign" placeholder="Item Name" id="name" name="name"  ref={register}/></div>
                                  

                            //         <div className="Conditions"><a>Condition</a> <select name="condition"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="NEW" > New </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="USED" > Used </option>
                                        
                            //             </select></div>
                            //         <div className="Pictures"> <a>Picture</a> <input  type="file" className="sign" placeholder="Picture" name="pic"  ref={register}/></div>
                            //         <div className="Prices"><a>Price</a> <input className="sign" placeholder="Price" name="price"  ref={register}/></div>
                                
                            //         <div className="Categorys"><a>Category</a> <select name="categoryId"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                                
                            //             {
                            //                 cat.map(category =>
                            //                     <option  className="sign" placeholder="Item Condition"  value={category._id} > {category.name} </option>
                            //                     )
                            //             }
                                        
                            //             </select></div>




                            //         <div className="Quantitys"> <a>Quantity</a> <input  className="sign" placeholder="Quantity" name="qty"  ref={register}/></div>
                            //         <div className="Descriptions"> <a>Description</a> <input  className="sign" placeholder="Description" name="description"  ref={register}/></div>

                                    

                                    
                                    
                            //         <div className="ReturnPolicys"><a>ReturnPolicy</a> <select name="returnPolicy"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="Yes" > 30 Days </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="No" > No </option>
                                        
                            //             </select></div>
                                
                                
                                

                                    
                                    
                                
                                
                                     
                            //          <input  type="image" className="signarrow" src= {require('../../Assets/icons/submits.svg')} alt = "image"  /> 
                            // </form>


                           

                            // </div>
                            // </div>


                           
                            <Form >
                            <Row>

                                <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Item Name</Form.Label>
                                        <Form.Control  className="sign-up" type="text" placeholder="Item Name" name="name"   value={Item.name}  onChange= {changeHandler} />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control  className="sign-up" type="text" placeholder="name@example.com" name="price" value={Item.price}  onChange= {changeHandler} />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Quantity</Form.Label>
                                        <Form.Control   className="sign-up" type="text" placeholder="name@example.com" name="qty" value={Item.qty}  onChange= {changeHandler}  />
                                        </Form.Group>
                                    <Form.Group className="exampleForm.ControlInput1">
                                    <Form.Label>Pic</Form.Label>
                                        <Form.Control   className="sign-up"  type="file" placeholder="name@example.com" name="pic"  onChange= {changeHandler,PicHandler}    />
                                    {/* <Form.Label>Pic</Form.Label>
                                    </Form.Group>
                                    <Form.Group className="sign-up"> */}
                                    {/* <input  type="file"  placeholder="Picture" name="pic"  ref={register}/> */}
                                        {/* <Form.File   
                                            id="custom-file"
                                            label="Add Item Pic"
                                            custom
                                            name="pic"  ref={register}
                                        /> */}
                                     </Form.Group>
                                </Col>

                                <Col  className="sign-up" xs={12} md={4}>
                                        <Image    src= {picture}  thumbnail/>
                                </Col>

                                
                          
                        </Row>
                       
                            
                            <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Condition</Form.Label>
                            <Form.Control  className="sign-up" as="select" name="condition" value={Item.condition}  onChange= {changeHandler}  >
                                        <option    value="NEW" > New </option>
                                         <option   value="USED" > Used </option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Return Policy</Form.Label>
                            <Form.Control className="sign-up" as="select" name="returnPolicy" value={Item.returnPolicy}   onChange= {changeHandler}  >
                                <option    value="Yes" > 30 Days </option>
                             <option   value="No" > No </option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Category</Form.Label>
                            <Form.Control  className="sign-up" as="select"name="categoryId"  value={Item.categoryId}  onChange= {changeHandler} >
                           
                            
                                      {
                                           cat.map(category =>
                                              <option    value={category._id} > {category.name} </option>
                                               )
                                        }
                            </Form.Control>
                            </Form.Group>
                            
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description</Form.Label>
                            <Form.Control  className="sign-up" as="textarea" rows="3" name="description" value={Item.description}   onChange= {changeHandler}  />
                            </Form.Group>
                            <Row>
                            <Col>
                            
                                        <Row className="justify-content-start">

                                        
                                              <Button  className="justify-content-center" onClick={()=>window.location.reload(false)} variant="dark">Back</Button>
                                        </Row>
                                        </Col >
                            <Col>
                            <Row className="justify-content-end">
                                <Button   onClick={OnSubmit} variant="dark">Submit</Button>
                            </Row>
                            
                            </Col>
                            </Row>
                           
                           
                      </Form>
    )
}

     










