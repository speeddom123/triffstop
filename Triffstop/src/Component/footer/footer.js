import React from 'react'
import "./style.css";
import {Container,Row,Col, Image} from 'react-bootstrap'
export default function Footer() {
    return (
        <Container className="footerCon footInfo" fluid>
            <Row className="justify-content-center  " fluid >
                    {/* <div className="footInfo">
                    <div className="footer"> */}
                        <Col sm md lg="3"  className="justify-content-md-center" >
                            <div className="footTitle">
                                        <a className="">Customer Service</a>
                                        <div className="fotinfo">
                                            <a>Help Center</a>
                                            <a>Contact US</a>
                                        </div>

                                </div>
                        </Col>
                        <Col  sm md lg="3" >
                                <div className="footTitle">
                                            <a className="footTitle">About US</a>
                                                    <div  className="fotinfo">
                                                        <a>About TriffStop</a>
                                                        <a>Site Map</a>
                                                    </div>

                                    </div>

                        </Col>
                            
                           <Col  sm md lg="3" >
                                <div className="footTitle">
                                            <a className="footTitle">Buy On TriffStop</a>
                                                    <div  className="fotinfo">
                                                        <a>Policy</a>
                                                        <a>License</a>
                                                    </div>


                                    </div>
                           </Col> 
                            <Col  sm md lg="3">
                                        <div className="footTitle">
                                                <a className="footTitle">Sell On TriffStop</a>
                                                        <div  className="fotinfo">
                                                            <a>Policy</a>
                                                            <a>License</a>
                                                        </div>




                                        </div>
                            
                            </Col>
                            
                    {/* </div>
                    
                </div> */}
            </Row>

            <Row className="footInfo">
            <div className="copyright">
                            <a>© 2020 Copyright: MDBootstrap.com</a>
                    </div>
            </Row>



        

        </Container>
        
    )
}
