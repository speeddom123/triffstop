import React from 'react'
import "./style.css";
import { Link } from 'react-router-dom';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";


const CatCard = (props) => {
    const photos =
    [
    {
        name: 'Photo 1',
        url:  require(`../../Assets/images/bnner/car.jpg`),
    },
    {
        name: 'Photo 2',
        url:  require(`../../Assets/images/bnner/car.jpg`),
    }
]
    const settings = {
        dots: false,
        fade: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        arrows: true,
        slidesToScroll: 1,
        className: "imageslides"
      };
   
  
   
    if(!props.item){
        return <h1>Loading...</h1>
    }
   console.log(props.color.color )
    return (
       
        <div className="itemContainer">
            <Link to={{ pathname:`/category/${props.item.name}/${props.item._id}` , id : props.item.id , color : props.color.color  }}>


          
            <div className="bgshadow" style={{filter: `drop-shadow(-1px 5px 4px ${props.color.color})`}} >

           
          <div  className="firstpic" style={props.color}>

          <div className="bottum"> <div><a style = {{fontWeight: "bold",fontSize: "18px",  color: "white"}}>{props.item.name}</a> </div>
          <div> <a style = {{fontWeight: "bold",fontSize: "10px"}} >200+ Phones</a>  </div>
          </div>
          </div>
           <div className="">
               
              
                            <div>
                                
                                 {/* <img className="slideimgCon" src={ require('../../Assets/images/recents/display.jpg')}></img> */}
                                 <img  className="slideimgCon"  src= {`/uploads/${props.item.pic}`} alt = "image" />
                            </div>
               
                            
                       
             
               
           </div>
           
           </div>        
           </Link>
       </div>  

   
    )
}

export default CatCard
