import React, { Component } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import "./style.css";
import { NavLink } from 'react-router-dom';
import {Container,Row,Col, Image,Form, Button, FormControl, NavDropdown, Nav} from 'react-bootstrap'
import Navbar from 'react-bootstrap/Navbar'
export const Navbars = () => {
  const login = useSelector((state) => state.appreducer.login);
  console.log(login,'login')
  if(login){
    return (
      // <Container className = "navC" fluid >
      //     <Row  className="align-items-center" fluid>
      //         <Col sm md lg="3" className="justify-content-md-center">
      //         <div className=""> <NavLink to="/"><img className="logopic" src= {require('../../Assets/icons/TriffStop.svg')} alt = "image" /></NavLink></div>
      //         </Col>
      //         <Col sm="12" md="12" lg="6">
      //         <div className="searchb"><input className="sbar"></input></div> 
      //         </Col>
              
      //         <Col sm mdlg="1">
      //         <div className="signs"><NavLink to="/sign-up"><img className="Signup" src= {require('../../Assets/navbar/SIGN UP.svg')} alt = "image" /></NavLink></div>
                 
      //         </Col>
      //         <Col sm md lg="1">
      //         <div className="login"> <NavLink to="/sign-in"><img className="Login" src= {require('../../Assets/navbar/LOG IN.svg')} alt = "image" /></NavLink></div>
      //         </Col>
              
                
      //     </Row>

      //  </Container>
      
      <Navbar  className = "navbar navbar-dark  navC" expand="lg">
<Navbar.Brand className="text-white " href="/"><h1>TriffStop</h1></Navbar.Brand>
<Navbar.Toggle variant="white" aria-controls="responsive-navbar-nav" />
<Navbar.Collapse variant="white" id="basic-navbar-nav">
  <Nav className="mr-auto ">
    <Nav.Link className="text-white " href="/User/Profile"><h4>Home</h4></Nav.Link>
    <Nav.Link className="text-white " href="/sign-up"><h4>Log Out</h4></Nav.Link>
    
  </Nav>
  <Form inline>
    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
    <Button variant="outline-light">Search</Button>
  </Form>
</Navbar.Collapse>
</Navbar>
  )
  }else{
    return (
      // <Container className = "navC" fluid >
      //     <Row  className="align-items-center" fluid>
      //         <Col sm md lg="3" className="justify-content-md-center">
      //         <div className=""> <NavLink to="/"><img className="logopic" src= {require('../../Assets/icons/TriffStop.svg')} alt = "image" /></NavLink></div>
      //         </Col>
      //         <Col sm="12" md="12" lg="6">
      //         <div className="searchb"><input className="sbar"></input></div> 
      //         </Col>
              
      //         <Col sm mdlg="1">
      //         <div className="signs"><NavLink to="/sign-up"><img className="Signup" src= {require('../../Assets/navbar/SIGN UP.svg')} alt = "image" /></NavLink></div>
                 
      //         </Col>
      //         <Col sm md lg="1">
      //         <div className="login"> <NavLink to="/sign-in"><img className="Login" src= {require('../../Assets/navbar/LOG IN.svg')} alt = "image" /></NavLink></div>
      //         </Col>
              
                
      //     </Row>

      //  </Container>
      
      <Navbar  className = "navbar navbar-dark  navC" expand="lg">
<Navbar.Brand className="text-white " href="/"><h1>TriffStop</h1></Navbar.Brand>
<Navbar.Toggle variant="white" aria-controls="responsive-navbar-nav" />
<Navbar.Collapse variant="white" id="basic-navbar-nav">
  <Nav className="mr-auto ">
    <Nav.Link className="text-white " href="/sign-in"><h4>Login</h4></Nav.Link>
    <Nav.Link className="text-white " href="/sign-up"><h4>Signup</h4></Nav.Link>
    
  </Nav>
  <Form inline>
    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
    <Button variant="outline-light">Search</Button>
  </Form>
</Navbar.Collapse>
</Navbar>
  )
  }
   
}



export default Navbars
