
import axios from 'axios';
// import "./style.css";
import {useForm} from 'react-hook-form';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom/withRouter";

import {Form,Row,Col, Button, Image} from 'react-bootstrap'
import React from 'react';



import { useState,useEffect } from 'react';


export default function ContactSeller(props) {
    const [Item, setItem] = useState({});
    const [picture, setPicture] = useState({});
    const [seller, setseller] = useState({});
     
        useEffect(async () => {
           
            let  sellerdata = sessionStorage.getItem('seller');
            sellerdata= JSON.parse(sellerdata);
            setseller(sellerdata);
            console.log(seller);
            
        if(!props.item){
            console.log("working")
            let data = sessionStorage.getItem('Item');
            data= JSON.parse(data);
      
           
            setItem(data)
            setPicture( `/uploads/${data.pic}`);
         ;
        }else{
            sessionStorage.setItem('Item',JSON.stringify(props.item) )

            let data = sessionStorage.getItem('Item');
            data= JSON.parse(data);
      
           
            setItem(data)
            setPicture( `/uploads/${data.pic}`);
          
        }
          }, []);
   
    const {register, handleSubmit, errors} = useForm();
    const submitHandler = (data) =>
    {    console.log(seller)
          data.sellerId = seller._id;
         data.itemId = props.item._id;
         console.log(data)
        axios.post('/contact', data)
            .then( Response => {
                console.log(Response.data)
                
            }).catch(error =>{
                console.log(error)
            
            })
    }


    
    
    return (
        

                            // <div className="log-con">
                            // <div className="logInUserContainer">
                            // <div className="signUpTitle">
                            //         <a >ADD ITEMS</a>
                            // </div>
                            // <form className="addItem"  onSubmit={handleSubmit(OnSubmit)} >
                                    
                            //         <div className="Names"><a>Name</a> <input className="sign" placeholder="Item Name" id="name" name="name"  ref={register}/></div>
                                  

                            //         <div className="Conditions"><a>Condition</a> <select name="condition"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="NEW" > New </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="USED" > Used </option>
                                        
                            //             </select></div>
                            //         <div className="Pictures"> <a>Picture</a> <input  type="file" className="sign" placeholder="Picture" name="pic"  ref={register}/></div>
                            //         <div className="Prices"><a>Price</a> <input className="sign" placeholder="Price" name="price"  ref={register}/></div>
                                
                            //         <div className="Categorys"><a>Category</a> <select name="categoryId"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                                
                            //             {
                            //                 cat.map(category =>
                            //                     <option  className="sign" placeholder="Item Condition"  value={category._id} > {category.name} </option>
                            //                     )
                            //             }
                                        
                            //             </select></div>




                            //         <div className="Quantitys"> <a>Quantity</a> <input  className="sign" placeholder="Quantity" name="qty"  ref={register}/></div>
                            //         <div className="Descriptions"> <a>Description</a> <input  className="sign" placeholder="Description" name="description"  ref={register}/></div>

                                    

                                    
                                    
                            //         <div className="ReturnPolicys"><a>ReturnPolicy</a> <select name="returnPolicy"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="Yes" > 30 Days </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="No" > No </option>
                                        
                            //             </select></div>
                                
                                
                                

                                    
                                    
                                
                                
                                     
                            //          <input  type="image" className="signarrow" src= {require('../../Assets/icons/submits.svg')} alt = "image"  /> 
                            // </form>


                           

                            // </div>
                            // </div>


                            <Form style = {{padding:"5%"}} onSubmit={handleSubmit(submitHandler)}>
                            <Row>

                                <Col   xs={{ span: 12, order: 2 }} sm={{ span: 12, order: 2 }}   md={{  order: 1 }} lg={{  order: 1 }}>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Item Name: </Form.Label>
                                        &nbsp;
                                        <Form.Label> <h5>{Item.name}</h5></Form.Label>
                                       
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Item Price: </Form.Label>
                                        &nbsp;
                                        <Form.Label> <h5>${Item.price}</h5></Form.Label>
                                       
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control  className="sign-up" type="email" placeholder="name@example.com" name="email"  ref={register}/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Message</Form.Label>
                                        <Form.Control   className="sign-up" as="textarea" rows="3"  name="message"  ref={register} />
                                        </Form.Group>
                                        <Button className="buttonSignUp" variant="dark" type="submit">
                                Submit
                            </Button>
                                </Col>

                                <Col   xs={{ span: 12, order: 1 }} sm={{ span: 12, order: 1 }} md="5"  lg ="5">
                                        <Image  className="sign-up"  src= {picture}  thumbnail/>
                                </Col>
                                
                                
                          
                        </Row>
                       
                            
                            
                      </Form>
    )
}

     










