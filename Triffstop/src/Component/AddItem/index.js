
import axios from 'axios';
import "./style.css";
import {useForm} from 'react-hook-form';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom/withRouter";

import {Form,Row,Col, Button, Image} from 'react-bootstrap'
import React from 'react';



import { useState,useEffect } from 'react';


export default function HunterSignUp(props) {
    const [cat, setCat] = useState([]);
    const [picture, setPicture] = useState({});
    
      useEffect(async () => {
        const result = await axios.get(
            '/category'
        );
       
        setCat(result.data);
      }, []);
    const {register, handleSubmit, errors} = useForm();
    

const    changeHandler = e =>{
     setPicture(URL.createObjectURL(e.target.files[0])) 
     
      
      
    }
    const OnSubmit = (data) =>{
        console.log(cat);
     
        console.log(data);  
        let datas = sessionStorage.getItem('user');
        const user= JSON.parse(datas);
        console.log(user);
        data.userId = user._id;
        data.pic =data.pic[0]
        const fd = new FormData()
        fd.append('name', data.name)
        fd.append('pic', data.pic, data.pic.name)
        fd.append('condition', data.condition)
        fd.append('description', data.description)
        fd.append('returnPolicy', data.returnPolicy)
        fd.append('qty', data.qty)
        fd.append('price', data.price)
        fd.append('userId', data.userId)
        fd.append('categoryId', data.categoryId)
        fd.get('name')
        console.log(fd.get('name'));  
        console.log(fd);  
        
       axios.post( '/item/',fd ,{
            onUploadProgress : progressEvent=> {
                console.log(" Upload Progress: "  + (progressEvent.loaded / progressEvent.total *100) + '%')
            }
         ,
            headers: {
              'Authorization': `token ${user.token}`,
              'Content-Type': 'multipart/form-data'
            } },
               
             ).then( Response => {
            console.log(Response.data)
            window.location.reload(false);
            
           
        }).catch(error =>{
            console.log(error)
        })
       // window.location.href = '/huntersHub'
        //<Redirect to="/"  />
       //<Link className="link" to='/huntersHub'/>*/
    }
    
    return (
        

                            // <div className="log-con">
                            // <div className="logInUserContainer">
                            // <div className="signUpTitle">
                            //         <a >ADD ITEMS</a>
                            // </div>
                            // <form className="addItem"  onSubmit={handleSubmit(OnSubmit)} >
                                    
                            //         <div className="Names"><a>Name</a> <input className="sign" placeholder="Item Name" id="name" name="name"  ref={register}/></div>
                                  

                            //         <div className="Conditions"><a>Condition</a> <select name="condition"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="NEW" > New </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="USED" > Used </option>
                                        
                            //             </select></div>
                            //         <div className="Pictures"> <a>Picture</a> <input  type="file" className="sign" placeholder="Picture" name="pic"  ref={register}/></div>
                            //         <div className="Prices"><a>Price</a> <input className="sign" placeholder="Price" name="price"  ref={register}/></div>
                                
                            //         <div className="Categorys"><a>Category</a> <select name="categoryId"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                                
                            //             {
                            //                 cat.map(category =>
                            //                     <option  className="sign" placeholder="Item Condition"  value={category._id} > {category.name} </option>
                            //                     )
                            //             }
                                        
                            //             </select></div>




                            //         <div className="Quantitys"> <a>Quantity</a> <input  className="sign" placeholder="Quantity" name="qty"  ref={register}/></div>
                            //         <div className="Descriptions"> <a>Description</a> <input  className="sign" placeholder="Description" name="description"  ref={register}/></div>

                                    

                                    
                                    
                            //         <div className="ReturnPolicys"><a>ReturnPolicy</a> <select name="returnPolicy"  ref={register}> 
                            //         <option  className="sign" placeholder="Item Condition"  value='' >  </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="Yes" > 30 Days </option>
                            //             <option  className="sign" placeholder="Item Condition"  value="No" > No </option>
                                        
                            //             </select></div>
                                
                                
                                

                                    
                                    
                                
                                
                                     
                            //          <input  type="image" className="signarrow" src= {require('../../Assets/icons/submits.svg')} alt = "image"  /> 
                            // </form>


                           

                            // </div>
                            // </div>


                            <Form onSubmit={handleSubmit(OnSubmit)}>
                            <Row>

                                <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Item Name</Form.Label>
                                        <Form.Control  className="sign-up" type="text" placeholder="Item Name" name="name"  ref={register}/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control  className="sign-up" type="text" placeholder="name@example.com" name="price"  ref={register}/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Quantity</Form.Label>
                                        <Form.Control   className="sign-up" type="text" placeholder="name@example.com" name="qty"  ref={register} />
                                        </Form.Group>
                                    <Form.Group className="exampleForm.ControlInput1">
                                    <Form.Label>Pic</Form.Label>
                                        <Form.Control   className="sign-up"  type="file" placeholder="name@example.com" name="pic"  onChange= {changeHandler} ref={register} />
                                    {/* <Form.Label>Pic</Form.Label>
                                    </Form.Group>
                                    <Form.Group className="sign-up"> */}
                                    {/* <input  type="file"  placeholder="Picture" name="pic"  ref={register}/> */}
                                        {/* <Form.File   
                                            id="custom-file"
                                            label="Add Item Pic"
                                            custom
                                            name="pic"  ref={register}
                                        /> */}
                                     </Form.Group>
                                </Col>

                                <Col  className="sign-up" xs={12} md={4}>
                                        <Image    src= {picture}  thumbnail/>
                                </Col>

                                
                          
                        </Row>
                       
                            
                            <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Condition</Form.Label>
                            <Form.Control  className="sign-up" as="select" name="condition"  ref={register}>
                                        <option    value="NEW" > New </option>
                                         <option   value="USED" > Used </option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Return Policy</Form.Label>
                            <Form.Control className="sign-up" as="select" name="returnPolicy"  ref={register} >
                                <option    value="Yes" > 30 Days </option>
                             <option   value="No" > No </option>
                            </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                            <Form.Label>Category</Form.Label>
                            <Form.Control  className="sign-up" as="select"name="categoryId"  ref={register} >
                           
                            
                                      {
                                           cat.map(category =>
                                              <option    value={category._id} > {category.name} </option>
                                               )
                                        }
                            </Form.Control>
                            </Form.Group>
                            
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description</Form.Label>
                            <Form.Control  className="sign-up" as="textarea" rows="3" name="description"  ref={register} />
                            </Form.Group>
                            <Row>
                            <Col>
                            
                                        <Row className="justify-content-start">

                                        
                                              <Button  className="justify-content-center" onClick={()=>window.location.reload(false)} variant="dark">Back</Button>
                                        </Row>
                                        </Col >
                            <Col>
                            <Row className="justify-content-end">
                                <Button   type="submit" variant="dark">Submit</Button>
                            </Row>
                            
                            </Col>
                            </Row>
                            
                      </Form>
    )
}

     










