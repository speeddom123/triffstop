import {combineReducers} from 'redux'
import {appReducer} from './reducer/app'
import {cartReducer} from './reducer/cart'

  
const reducer = combineReducers({
    appreducer: appReducer,
    cartReducer: cartReducer
})
export default reducer;