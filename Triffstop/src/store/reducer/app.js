
// import {app} from './reducers/app'

const initialState = {
    login: false,
    user:{}
  };
  
  export  const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'login': {
            return {
              ...state,
              login: true,
              user: action.payload
            };
          }
      default:
        return state;
    }
  };
  


  